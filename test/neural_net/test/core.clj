(ns neural-net.test.core
  (:use [neural-net.core] :reload)
  (:use [clojure.test]))

(deftest run-simple-neuron
  (is 2 (run {:phi     identity
              :accum   (comp (partial reduce +) (partial map *))
              :weights [1 1]}
             [1 1]))
  (is 0.5 (run {:phi identity
                :accum (comp (partial reduce +) (partial map (fn [x w] (* x w))))
                :weights [0.5 1]}
               [1 0])))

(deftest output-numbers
  (let [n {:weights [1 2 1 4 1]}]
    (is 1 (outputs [n (list n n) n]))
    (is 4 (outputs (list n (list n n) n)))
    (is 5 (inputs [n (list n n) n]))
    (is 5 (inputs (list n (list n n) n)))))

(deftest front-page-examples
  (is 2 (run {:phi     identity
              :accum   (comp (partial reduce +) (partial map *))
              :weights [1 1]}
             [1 1]))
  (is 4 (let [n {:phi identity
                 :accum (comp (partial reduce +) (partial map *))}
              net [(assoc n :weights [1 1]) (assoc n :weights [2])]]
          (run net [1 1])))
  (is 36 (let [n {:phi identity
                  :accum (comp (partial reduce +) (partial map *))
                  :weights [2 2 2]}
               net [(list n n n) n]]
           (run net [1 1 1]))))
