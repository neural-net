(ns neural-net.som                      ; Kohonen maps
  (:use neural-net.core)
  (:use clojure.set))

(defn self-organizing-map:run
  "Return a map of the dot product of each neurons weight against x" [this x]
  (reduce (fn [a [k v]] (cons [k (reduce + (map * x (v :weights)))] a))
          [] (get (this :map) :vertices)))

(defn self-organizing-map:learn
  "Return a map of learning spread through the map." [this x y d]
  (let [winner (first (first (reverse (sort-by second (or y (run this x))))))]
    (reduce (fn [a [n d]] (assoc a n ((this :update) this d n x)))
            {} (get (this :nn) winner))))

(defn self-organizing-map:train [this delta]
  (assoc this
    :map (neural-net.core.Graph.
          (reduce (fn [a [k d]] (assoc a
                                 k {:weights (map + d ((get a k) :weights))}))
                  (get (this :map) :vertices) delta)
          (get (this :map) :edges))))

(comment                                ; train a self organizing map
  (let [som-g (neural-net.core.Graph.
               {:a {:weights [ 1  0]}
                :b {:weights [ 0  1]}
                :c {:weights [-1  0]}
                :d {:weights [ 0 -1]}}
               [[:a :b] [:b :c] [:c :d]])
        som {:phi self-organizing-map:run
             :learn self-organizing-map:learn
             :train self-organizing-map:train
             :eta 1
             :update (fn [this dist n x]
                       (map (fn [x] (* (/ x (inc dist)) (this :eta)))
                            (map -
                                 x (((get (this :map) :vertices) n) :weights))))
             :map som-g
             :nn (nearest-neighbors som-g)}]
    (first (train som [0.8 0.2] nil nil)))
  ;; =>
  ;; {:d (0.2 0.3),  :c (0.6 0.06666666666666667),
  ;;  :b (0.4 -0.4), :a (-0.19999999999999996 0.2)}  
  )
