(ns neural-net.util
  (:use neural-net.core)
  (:use clojure.contrib.math))

(defn pick
  ([lst] (nth lst (rand (count lst))))
  ([lst num]
     (if (> num 0)
       (let [i (rand (count lst))]
         (cons (nth lst i)
               (pick (concat (take (dec i) lst) (drop i lst)) (dec num))))
       nil)))

(defn rand-weight [] (- (rand 2) 1))

(defn mean [lst] (if (empty? lst) 0 (/ (reduce + lst) (count lst))))

(defn stddev [lst]
  (let [m (mean lst)]
    (sqrt (/ (reduce + (map (fn [el] (expt (- m el) 2)) lst))
             (dec (count lst))))))

(defn rms-error "Collect the error from an epic"
  ([net epic] (rms-error net epic identity))
  ([net epic pull]
     (sqrt (/ (reduce + (map (fn [[x d]] (expt (- (pull (run net x)) d) 2)) epic))
              (* (count epic) (if (coll? (second (first epic)))
                                (count (second (first epic)))
                                1))))))
