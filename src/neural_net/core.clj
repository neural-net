(ns neural-net.core
  (:use clojure.set))

(defrecord Graph [#^clojure.lang.IPersistentMap vertices
                  #^clojure.lang.IPersistentVector edges])

(defn -neighbors [#^neural-net.core.Graph g v]
  (keep identity (map (fn [[a b]] (cond (= a v) b (= b v) a)) (get g :edges))))

(def neighbors (memoize -neighbors))

(defn nearest-neighbors
  ([#^neural-net.core.Graph g] (nearest-neighbors g (count (get g :vertices))))
  ([#^neural-net.core.Graph g max]
     (let [edges  (get g :edges)
           vs     (keys (get g :vertices))
           num-vs (count (keys (get g :vertices)))]
       (reduce (fn [a v]
                 (assoc a
                   v (loop [now (set [v]) dist 0 added 0 out {}]
                       (let [new (set (mapcat (partial neighbors g) now))]
                         (if (or (> dist max) (>= added num-vs))
                           out
                           (recur (difference new (keys out))
                                  (inc dist)
                                  (+ added (count now))
                                  (reduce (fn [a v] (assoc a v dist))
                                          out now)))))))
               {} vs))))

(defn deps [#^neural-net.core.Graph g]
  (reduce (fn [o v] (assoc o v (set (map first (filter (fn [[a b]] (= v b))
                                                      (:edges g))))))
          {} (keys (:vertices g))))

(defn fold-deps [f a #^neural-net.core.Graph g]
  (let [deps (deps g)]
    (loop [vts (set (keys (:vertices g))) a a d '()]
      (let [n (first (filter (fn [v] (subset? (get deps v) d)) vts))]
        (if (or (empty? vts) (not n)) a
            (recur (difference vts #{n}) (f a n) (union #{n} d)))))))

(defprotocol Neural     "Protocol implemented by elements of a neural network."
  (run     [this x]     "Evaluates the net")
  (train   [this x y d] "Trains the net returning the updated net and deltas")
  (collect [this key]   "Collect key from the network")
  (inputs  [this]       "Number of inputs")
  (outputs [this]       "Number of outputs")
  (check   [this]       "Ensure that the number of inputs matches the outputs"))

(extend-protocol
 Neural
 clojure.lang.IPersistentMap ; a map initializes a single neural element
 (run     [this x]     ((this :phi) this x))
 (train   [this x y d] ((fn [delta] [delta ((this :train) this delta)])
                        ((this :learn) this x (or y (run this x)) d)))
 (collect [this key]   (this key))
 (inputs  [this]       (count (this :weights)))
 (outputs [this]       1)
 (check   [this]       nil)
 clojure.lang.ISeq               ; a list of many ns in the same layer
 (run     [this x]     (vec (map (fn [n] (run n x)) this)))
 (train   [this x y d]
          (let [trained (map (fn [n d] (train n x (run n x) d)) this d)]
            [(vec (apply map (comp vec list) (vec (map first trained))))
             (map second trained)]))
 (collect [this key]   (map (fn [el] (collect el key)) this))
 (inputs  [this]       (apply max (map inputs this)))
 (outputs [this]       (reduce + (map outputs this)))
 (check   [this]
          (filter identity
                  (map-indexed (fn [i err] (when (not (empty? err)) {i err}))
                               (map check this))))
 clojure.lang.IPersistentVector   ; a vector of ns in different layers
 (run     [this x]
          (reduce (fn [x layer] (run layer (if (vector? x) x [x]))) x this))
 (train   [this x y d]
          (let [xs (reverse (reduce
                             (fn [xs layer] (cons (run layer (first xs)) xs))
                             [x] this))
                trained (reduce (fn [ds [x n y]]
                                  (cons (train n x y (first (first ds))) ds))
                                [(cons d nil)]
                                (reverse (map list xs this (rest xs))))]
            [(first (first trained)) (vec (map second (butlast trained)))]))
 (collect [this key] (vec (map (fn [el] (collect el key)) this)))
 (inputs  [this]     (inputs (first this)))
 (outputs [this]     (outputs (last this)))
 (check   [this]
          (let [boundries (map (fn [el] [(inputs el) (outputs el)]) this)]
            (filter identity
                    (map-indexed
                     (fn [i [a b]] (when (not (= (second a) (first b)))
                                    {:at i :from (second a) :to (first b)}))
                     (map (fn [a b] [a b]) boundries (rest boundries))))))
 neural-net.core.Graph ; a Graph allows arbitrary network architectures
 (run [this x]
      (let [deps (deps this)
            res ((fold-deps
                  (fn [a n]
                    (let [ni (inputs (get (:vertices this) n))
                          in (concat (map (partial get (a :v)) (get deps n))
                                     (take (- ni (count (get deps n))) (a :x)))]
                      (assoc a
                        :v (assoc (a :v) n (run (get (:vertices this) n) in))
                        :x (drop (- ni (count (get deps n))) (a :x)))))
                  {:v {} :x x} this) :v)]
        (vec (map #(res %) (difference (set (keys (:vertices this)))
                                       (set (map first (:edges this))))))))
 (train
  [this x y d]
  (let [rdeps (deps (Graph. (:vertices this) (map reverse (:edges this))))
        deps (deps this)
        xs ((fold-deps
             (fn [a n]
               (let [ni (inputs ((:vertices this) n))
                     in (vec (concat (map (comp last (partial get (a :v))) (get deps n))
                                     (take (- ni (count (get deps n))) (a :x))))]
                 (assoc a
                   :v (assoc (a :v) n [in (run ((:vertices this) n) in)])
                   :x (drop (- ni (count (get deps n))) (a :x)))))
             {:v {} :x x} this) :v)]
    ((fn [trained]
       [(vec (flatten (map first (vals trained))))
        (Graph. (apply hash-map (mapcat (fn [[n [a b]]] [n b]) trained))
                (:edges this))])
     ((fold-deps
       (fn [a n]
         (assoc a
           :v (reduce
               (fn [o m] (assoc o m ((fn [[a b]] [(vec (butlast a)) b]) (o m))))
               (assoc (a :v)
                 n (train
                    ((:vertices this) n)
                    (first (get xs n))
                    (last (get xs n))
                    (#(if (= (outputs ((:vertices this) n)) 1) (first %) %)
                     (concat (map (comp last first (partial get (a :v))) (get rdeps n))
                             (a :d)))))
               (get rdeps n))
           :d (if (empty? (get rdeps n)) (rest (a :d)) (a :d))))
       {:v {} :d d} (Graph. (:vertices this) (map reverse (:edges this)))) :v))))
 (collect [this key])
 (inputs [this] (- (reduce + (map (comp inputs second) (:vertices this)))
                   (count (:edges this))))
 (outputs [this] (reduce + (map (comp outputs (partial get (:vertices this)))
                                (difference (set (keys (:vertices this)))
                                            (set (map first (:edges this)))))))
 (check [this]
        (filter identity
                (map
                 (fn [[k v]]
                   (let [need (inputs v)
                         supp (reduce +
                                      (map (fn [[a b]]
                                             (if (= b k)
                                               (outputs (get (:vertices this) a))
                                               0))
                                           (:edges this)))]
                     (when (> supp need) {:at k :in supp :out need})))
                 (:vertices this)))))
