(ns neural-net.back-propagation
  (:use neural-net.core))

(defn back-prop:run "Phi function for back propagation" [this x]
  (assert (this :weights))
  (assert (first (map :y x)))
  (let [v (reduce + (map (fn [x w] (* x w)) (map :y x) (this :weights)))]
    {:v v :y v}))

(defn back-prop:learn "Back propagation" [this x res d]
  ((fn [gradient]
     (vec (map
           (fn [y w]
             {:delta-w (* (this :eta) gradient y)
              :weight w
              :gradient gradient})
           (map :y x) (this :weights))))
   (if (and (map? d) (get d :desired))
     (* (- (d :desired) (res :y))       ; output layer
        ((this :d-phi) (res :v)))
     (* ((this :d-phi) (get res :v 1))  ; hidden layer
        (reduce + (map (fn [a] (* (a :gradient) (a :weight)))
                       (if (vector? d) d (vec (list d)))))))))

(comment       ; learning binary representations with back-propagation
  (let [n {:phi   back-prop:run
           :d-phi (fn [_] 1)
           :learn back-prop:learn
           :train (fn [n delta] (assoc n :weights
                                      (vec (map + (n :weights)
                                                (map :delta-w delta)))))
           :eta 0.1
           :weights [0.5 0.5 0.5]}
        epic [[[0 0 0] 0]
              [[0 0 1] 1]
              [[0 1 0] 2]
              [[0 1 1] 3]
              [[1 0 0] 4]
              [[1 0 1] 5]
              [[1 1 0] 6]
              [[1 1 1] 7]]
        net [(repeat 3 n) n]
        trained (reduce
                 (fn [m _]
                   (reduce
                    (fn [n p]
                      (let [x (vec (map (fn [el] {:y el}) (first p)))
                            d {:desired (second p)}]
                        (second (train n x (run n x) d))))
                    m epic)) net (range 20))]

    (map
     (fn [[in out]]
       [in out ((run trained (vec (map (fn [el] {:y el}) in))) :y)])
     epic))
  ;; =>
  ;; ([[0 0 0] 0 0.0]
  ;;  [[0 0 1] 1 1.0000000325609912]
  ;;  [[0 1 0] 2 2.0000000163062657]
  ;;  [[0 1 1] 3 3.000000048867257]
  ;;  [[1 0 0] 4 3.9999998651699054]
  ;;  [[1 0 1] 5 4.999999897730897]
  ;;  [[1 1 0] 6 5.999999881476171]
  ;;  [[1 1 1] 7 6.999999914037161])  
  )