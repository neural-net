(ns neural-net.activation-functions
  (:use [clojure.contrib math]))

;;; activation functions
(def d-step
     ^{:doc "Derivative of the step function."}
     (fn [v] (if (= v 0) (/ 1 0) 0)))

(def step
     ^{:doc "The step function." :deriv d-step}
     (fn [v] (if (pos? v) 1 -1)))

(def d-sigmoid
     ^{:doc "Derivative of the sigmoid function."}
     (fn [v] (/ (expt Math/E (- 0 v)) (expt (+ (expt Math/E (- 0 v)) 1) 2))))

(def sigmoid
     ^{:doc "The sigmoid function." :deriv d-sigmoid}
     (fn [v] (/ 1 (+ 1 (expt Math/E (- 0 v))))))
