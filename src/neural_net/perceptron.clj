(ns neural-net.perceptron
  (:use neural-net.core))

(defn perceptron:run "Perceptron execution" [this x]
  (reduce + (map (fn [x w] (* x w)) x (this :weights))))

(defn perceptron:learn "Perceptron learning" [this x y d]
  (let [dir ((fn [dif] (cond (> dif 0) 1 (< dif 0) -1 true 0)) (- d y))]
    (vec (map (fn [x] (* dir (this :eta) x)) x))))

(comment         ; perceptron learning -- converting binary to decimal
  (let [n {:phi perceptron:run
           :weights [0 0]
           :learn perceptron:learn
           :train (fn [n delta]
                    (assoc n :weights (vec (map + (n :weights) delta))))
           :eta 0.01}
        epic [[[0 0] 0]
              [[0 1] 1]
              [[1 0] 2]
              [[1 1] 3]]
        x [1 1]
        d 1]
    ((reduce (fn [m _]
               (reduce
                (fn [n p]
                  (second (train n (first p) (run n (first p)) (second p))))
                m epic)) n (range 100))
     :weights))              ; [2.0000000000000013 1.0000000000000007]
  )

(comment                 ; perceptron learning over an arbitrary graph
  (let [n {:phi perceptron:run
           :learn perceptron:learn
           :eta 0.01
           :train (fn [n delta]
                    (assoc n :weights (vec (map + (n :weights) delta))))
           :weights [2 2]}
        g (Graph. {:a n :b n :c n} [[:a :b] [:a :c] [:b :c]])
        x [1 1 1]
        d [30]]
    (list (run g x)
          (run (second (train g x nil d)) x)))
  )

