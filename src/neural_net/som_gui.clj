(ns neural-net.som-gui
  (:use neural-net.som)
  (:use neural-net.core)
  (:import (java.awt Color Graphics Dimension)
           (java.awt.image BufferedImage)
           (javax.swing JPanel JFrame)))

;; gui code evolved from example in Rich Hickey's Ants demo
;; (see http://clojure.googlegroups.com/web/ants.clj)

(def scale (ref 25))
(def dim   (ref 25))

(def pt (ref nil))
(def net (ref nil))

(defn render-net [#^Graphics o]
  (when @net
    (let [g (@net :map)
          x (comp (partial + (/ (* @scale @dim) 2)) (partial * @scale) first)
          y (comp (partial + (/ (* @scale @dim) 2)) (partial * @scale) (partial - 0) second)]
      (dorun                            ; draw neurons
       (for [w (map :weights (vals (get g :vertices)))]
         (doto o (.setColor (. Color black))
               (.drawOval (x w) (y w) 5 5)
               (.fillOval (x w) (y w) 5 5))))
      (dorun                              ; draw edges
       (for [[a b] (get g :edges)]
         (let [a ((get (get g :vertices) a) :weights)
               b ((get (get g :vertices) b) :weights )]
           (doto o (.setColor (. Color black))
                 (.drawLine (x a) (y a) (x b) (y b)))))))))

(defn render-pt [#^Graphics o]
  (when @pt
    (let [g (@net :map)
          x (comp (partial + (/ (* @scale @dim) 2)) (partial * @scale) first)
          y (comp (partial + (/ (* @scale @dim) 2)) (partial * @scale) (partial - 0) second)]
      (doto o (.setColor (. Color red))
            (.drawOval (x @pt) (y @pt) 5 5)
            (.fillOval (x @pt) (y @pt) 5 5)))))

(defn render [g]
  (let [img (new BufferedImage (* @scale @dim) (* @scale @dim)
                 (. BufferedImage TYPE_INT_ARGB))
        bg (. img (getGraphics))]
    (doto bg
      (.setColor (. Color white))
      (.fillRect 0 0 (. img (getWidth)) (. img (getHeight))))
    (render-net bg) (render-pt bg)
    (. g (drawImage img 0 0 nil))
    (. bg (dispose))))

(def animator (agent nil))

(def animation-sleep-ms (ref 100))

(def running true)

(defn start-gui []
  (def panel (doto (proxy [JPanel] []
                     (paint [g] (render g)))
               (.setPreferredSize (new Dimension
                                       (* @scale @dim)
                                       (* @scale @dim)))))
  (def frame (doto (new JFrame) (.add panel) .pack .show))
  (def animation (fn [x]
                   (when running (send-off *agent* #'animation))
                   (. panel (repaint))
                   (. Thread (sleep @animation-sleep-ms))
                   nil))
  (send-off animator animation))

(comment        ; usage -- set animator running then update pt and net

  (send-off animator animation)

  (let [som {:phi self-organizing-map:run
             :learn self-organizing-map:learn
             :train self-organizing-map:train
             :eta 0.5
             :update (fn [this dist n x]
                       (map (fn [x] (* (/ x (inc dist)) (this :eta)))
                            (map -
                                 x (((get (this :map) :vertices) n) :weights))))
             :map (neural-net.core.Graph.
                   {:a {:weights [ 2  0]}
                    :b {:weights [ 0  2]}
                    :c {:weights [-2  0]}
                    :d {:weights [ 0 -2]}
                    :e {:weights [ 2 -2]}
                    :f {:weights [ 4 -2]}
                    :g {:weights [ 8 -2]}}
                   [[:a :b] [:b :c] [:c :d] [:d :e] [:e :f] [:f :g]])}
        xs [[10 6] [14 8] [16 0] [12 8] [13 10] [14 12] [18 10] [15 14]
            [16 16] [12 -12] [12 -8] [10 -8] [0 0] [0 -4] [-4 0] [-6 -6]]]
    (dosync (ref-set net som))
    (dorun
     (map
      (fn [x]
        (dosync (ref-set pt x))
        (. Thread (sleep 500))
        (dosync (ref-set net (second (train @net x nil nil))))
        (. Thread (sleep 500)))
      xs)))
  )