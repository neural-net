(ns neural-net.k-means
  (:use neural-net.core)
  (:use neural-net.util)
  (:use clojure.contrib.math))

(defn euclid-dist [a b] (sqrt (reduce + (map (comp #(* % %) -) a b))))

(defn k-means-update
  "A single iteration of k-means clustering."  [centers samples]
  (reduce        ; reposition each center at the center of its samples
   (fn [a [center mine]]
     (assoc a [(mean (map first mine)) (mean (map second mine))] mine))
   {}
   (reduce                 ; group each sample with the nearest center
    (fn [centers sample]
      (let [closest (first (sort-by (partial euclid-dist sample)
                                    (keys centers)))]
        (assoc centers closest (cons sample (get centers closest '())))))
    (apply hash-map (mapcat (fn [c] [c '()]) centers))
    samples)))

(defn k-means [n samples]
  (loop [clst (k-means-update (pick samples n) samples)]
    (let [new-clst (k-means-update (keys clst) (mapcat identity (vals clst)))]
      (if (= (set (map set (vals new-clst))) (set (map set (vals clst))))
        (keys clst) (recur new-clst)))))
