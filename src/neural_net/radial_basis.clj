(ns neural-net.radial-basis
  (:use neural-net.core)
  (:use neural-net.util)
  (:use clojure.contrib.math))

(defn radial:run "Run a Gaussian radial basis neuron" [this x]
  (expt Math/E (/ (sqrt (reduce + (map (comp #(* % %) -) (this :t) x)))
                  (* 2 (sqrt (this :std))))))

(comment
  ;; sample data
  (def training                           
       (mapcat
        (fn [[c o]]
          (take 100 (repeatedly (fn []
                                  [(vec (map + c (take 2 (repeatedly rand))))
                                   0]))))
        [[[    0     0] 1]
         [[-0.75     0] 2]
         [[    0 -0.75] 3]
         [[-0.75 -0.75] 4]]))

  ;; run a radial basis neuron
  (let [x1s      (map (fn [[[x1 x2] o]] x1) training)
        x2s      (map (fn [[[x1 x2] o]] x2) training)
        std      (sqrt (/ (+ (expt (- (apply max x1s) (apply min x1s)) 2)
                             (expt (- (apply max x2s) (apply min x2s)) 2))
                          (sqrt 2)))
        rb       {:std std
                  :phi radial:run
                  :weights [1 1]}
        p        {:phi perceptron:run
                  :weights (vec (take 3 (repeatedly rand-weight)))
                  :learn perceptron:learn
                  :train (fn [n delta]
                           (assoc n :weights (vec (map + (n :weights) delta))))
                  :eta 0.01}]
    (run [(list (assoc rb :t [(pick x1s) (pick x2s)])
                (assoc rb :t [(pick x1s) (pick x2s)])
                (assoc rb :t [(pick x1s) (pick x2s)])) p]
         (first (first training))))

  ;; training is working... sort of
  (let [training (map (fn [p] [[(nth p 2) (nth p 3)] (nth p 0)]) training)
        testing (map (fn [p] [[(nth p 2) (nth p 3)] (nth p 0)]) testing)
        x1s (map (fn [[[x1 x2] o]] x1) training)
        x2s (map (fn [[[x1 x2] o]] x2) training)
        std (sqrt (/ (+ (expt (- (apply max x1s) (apply min x1s)) 2)
                        (expt (- (apply max x2s) (apply min x2s)) 2))
                     (sqrt 2)))
        rb {:std std
            :phi radial:run
            :weights [1 1]
            :learn (fn [this x y d] (map (fn [_] 0) (this :weights)))
            :train (fn [n delta] n)}
        p {:phi perceptron:run
           :weights (vec (take 3 (repeatedly rand-weight)))
           :learn perceptron:learn
           :train (fn [n delta]
                    (assoc n :weights (vec (map + (n :weights) delta))))
           :eta 0.00001}
        net [(list (assoc rb :t [(pick x1s) (pick x2s)])
                   (assoc rb :t [(pick x1s) (pick x2s)])
                   (assoc rb :t [(pick x1s) (pick x2s)])) p]]
    (reduce
     (fn [m index]
       (println (format "%S\t%S\t%S" index
                        (rms-error m training) (epic-error m testing)))
       (let [n (reduce
                (fn [n p]
                  (second (train n (first p) (run n (first p)) (second p))))
                m training)]
         n)) net (range 30)))
  )
